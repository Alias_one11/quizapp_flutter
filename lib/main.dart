import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import './models/quiz_brain.dart';

QuizBrain quiz = QuizBrain();

void main() => runApp(Quizzler());

class Quizzler extends StatelessWidget {
  @override
  Widget build(BuildContext context) => MaterialApp(
        home: Scaffold(
          backgroundColor: Colors.grey.shade900,
          body: SafeArea(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 10.0),
              child: QuizPage(),
            ),
          ),
        ),
      );
}
/*@-----------------------------------------------------------@*/

class _QuizPageState extends State<QuizPage> {
  /// - PROPERTIES
  List<Icon> scoreKeeper = [];

  @override
  Widget build(BuildContext context) => Column(
        //__________
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        //__________
        children: [
          //__________
          /* QUESTION WIDGET */
          questionComponent,
          //__________
          /* ANSWER-TRUE WIDGET */
          answerTrueComponent,
          //__________
          /* ANSWER-FALSE WIDGET */
          answerFalseComponent,
          /*SCORE KEEPER*/
          Row(children: scoreKeeper),
        ],
      );

  /// CLASS_COMPONENTS
  /// /*@-----------------------------------------------------------@*/
  Expanded get answerFalseComponent => Expanded(
        child: Padding(
          padding: EdgeInsets.all(15.0),
          child: FlatButton(
            //__________
            /*True-Button*/
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15),
            ),
            color: Colors.red,
            child: Text(
              'False',
              style: TextStyle(
                fontSize: 20.0,
                color: Colors.white,
              ),
            ),
            onPressed: () => answerUserPicked(false),
          ),
        ),
      );

  Expanded get answerTrueComponent => Expanded(
        child: Padding(
          padding: EdgeInsets.all(15.0),
          //__________
          /*True-Button*/
          child: FlatButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15),
            ),
            textColor: Colors.white,
            color: Colors.green,
            child: Text(
              'True',
              style: TextStyle(
                color: Colors.white,
                fontSize: 20.0,
              ),
            ),
            // Checking if the user got the answer, right or wrong
            onPressed: () => answerUserPicked(true),
          ),
        ),
      );

  Expanded get questionComponent => Expanded(
        flex: 5,
        child: Padding(
          padding: EdgeInsets.all(10.0),
          child: Center(
            child: Text(
              quiz.getQuestionText(),
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 25.0,
                color: Colors.white,
              ),
            ),
          ),
        ),
      );

  /// CLASS_METHODS
  /// /*@-----------------------------------------------------------@*/
  void answerUserPicked(bool answerUserPicked) {
    bool correctAnswer = quiz.getQuestionAnswer();

    setState(() {
      //__________
      if (quiz.restart() == true) {
        //__________
        Alert(
          context: context,
          title: 'END OF GAME!',
          desc: 'You\'ve reached the end of the game. Play again?',
          buttons: [
            DialogButton(
              child: Text(
                'Press To Play Again',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
              onPressed: () => Navigator.pop(context),
            ),
          ],
        ).show();

        // Resets the game
        quiz.reset();
        // Resets the icons
        scoreKeeper = [];
      } else {
        if (answerUserPicked == correctAnswer) {
          scoreKeeper.add(Icon(Icons.check, color: Colors.green));
        } else {
          scoreKeeper.add(Icon(Icons.close, color: Colors.red));
        }

        // Will go to the next question when the button pressed
        quiz.goToNextQuestion();
      }
      /*@-----------------------------------------------------------@*/
    });
  } // END OF METHOD

  /// END_>CLASS
}

/*@-----------------------------------------------------------@*/
class QuizPage extends StatefulWidget {
  @override
  _QuizPageState createState() => _QuizPageState();
}

/*
question1: 'You can lead a cow down stairs but not up stairs.', false,
question2: 'Approximately one quarter of human bones are in the feet.', true,
question3: 'A slug\'s blood is green.', true,
*/
