import '../models/question.dart';

class QuizBrain {
  /// - PROPERTIES
  var _questionIndex = 0;

  List<Question> _questionBank = [
    Question(
      question: 'Some cats are actually allergic to humans',
      answer: true,
    ),
    Question(
      question: 'You can lead a cow down stairs but not up stairs.',
      answer: false,
    ),
    Question(
      question: 'Approximately one quarter of human bones are in the feet.',
      answer: true,
    ),
    Question(
      question: 'A slug\'s blood is green.',
      answer: true,
    ),
    Question(
      question: 'Buzz Aldrin\'s mother\'s maiden name was \"Moon\".',
      answer: true,
    ),
    Question(
      question: 'It is illegal to pee in the Ocean in Portugal.',
      answer: true,
    ),
    Question(
      question:
          'No piece of square dry paper can be folded in half more than 7 times.',
      answer: false,
    ),
    Question(
      question:
          'In London, UK, if you happen to die in the House of Parliament, you are technically entitled to a state funeral, because the building is considered too sacred a place.',
      answer: true,
    ),
    Question(
      question:
          'The loudest sound produced by any animal is 188 decibels. That animal is the African Elephant.',
      answer: false,
    ),
    Question(
      question:
          'The total surface area of two human lungs is approximately 70 square metres.',
      answer: true,
    ),
    Question(
      question: 'Google was originally called \"Backrub\".',
      answer: true,
    ),
    Question(
      question:
          'Chocolate affects a dog\'s heart and nervous system; a few ounces are enough to kill a small dog.',
      answer: true,
    ),
    Question(
      question:
          'In West Virginia, USA, if you accidentally hit an animal with your car, you are free to take it home to eat.',
      answer: true,
    ),
  ];

  /* CLASS METHODS */
  /*@-----------------------------------------------------------@*/
  /// Getter methods
  String getQuestionText() => _questionBank[_questionIndex].question;
  bool getQuestionAnswer() => _questionBank[_questionIndex].answer;
  
  void goToNextQuestion() {
    // -1 makes sure it never goes out of index range
    if (_questionIndex < _questionBank.length - 1) {
      _questionIndex++;
    }
  }
  
  // Function that restarts the game if at the end of it
  bool restart() => _questionIndex >= _questionBank.length - 1 ? true : false;
  // Function that resets the questions back to the first question
  void reset() => _questionIndex = 0;

  /// END OF CLASS
}
