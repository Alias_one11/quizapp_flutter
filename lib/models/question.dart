//__________
class Question {
  /// - PROPERTIES
  String question;
  bool answer;

  // Constructor
  Question({this.question, this.answer});
}
